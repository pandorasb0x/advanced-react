# Advanced React

Este repo fue creado para el desarrollo del curso [Advanced React for Enterprise](https://www.udemy.com/course/react-for-senior-engineers/)

Y nos estaremos basando en este [FIGMA](https://www.figma.com/file/EX8VxcTtAatzI2PBLb361g/designsystems.engineering?node-id=99%3A0&t=lFF6ovtcoXsOiNRb-0)

# ¿Qué es un sistema de diseño o Design System?

Se trata de una librería en donde se guarda una colección de componentes reutilizables (y su código)para crear un producto rápidamente.

Está guiada por prácticas claras y preestablecidas, además de la esencia del producto, su filosofía, su porqué.

Es el ADN de nuestro producto y ayuda a que todo el equipo vaya en la misma dirección. No es estático, sino dinámico, y crece a medida que lo hace el producto.

Hay que tener en cuenta que un producto debe tener una coherencia, mucho más allá de la estética. No se trata únicamente de que un botón tenga el mismo border radius o que sus diferentes estados luzcan igual para la misma acción. No. Va mucho más allá. Tenemos que definir un lenguaje, cómo nos vamos a dirigir al usuario, qué tono vamos a usar. ¿Nos comunicaremos con ellos en primera o tercera persona? ¿Capitalizamos los label de los botones o no?

Lo que está claro es que decidamos lo que decidamos debemos seguir con la misma coherencia en el resto del producto y, por supuesto, medir continuamente para saber que esas decisiones son las acertadas.

Esto no solo afecta al equipo de diseño, también al resto de departamentos. Por ejemplo, para desarrollar un nuevo componente cada programador puede hacerlo de mil formas distintas, unas más eficaces y reutilizables (palabra clave) y otras menos. O cada miembro del proyecto puede nombrar un determinado componente de una manera distinta, creando confusión y malentendidos.

# ¿Qué es Atomic Design?

El concepto de Atomic Web Design surge del libro Atomic Design de Brad Frost, publicado por primera vez en 2013. Sus teorías se han convertido en un referente para la creación de los sistemas de diseño.

Brad Frost concibe el diseño de la interfaz web como un todo compuesto de unidades más pequeñas, que él llama átomos. Reutilizando y combinando estos componentes se generan elementos mayores, que son partes del diseño de un producto digital.

“Atomic design” es una metodología utilizada para construir sistemas de diseño efectivos y orientados al medio web o digital, inspirada en los principios de la Teoría General de Sistemas, que considera los sistemas vivos como sistemas abiertos, en constante interacción con otros sistemas.

Según su teoría, un sistema de diseño se compone de elementos de cinco niveles:

- ATOMOS:

  Son la unidad básica en “Atomic Design”, no pueden ser divididos en otras unidades, y componen los bloques más elementales de una interfaz de usuario.

  Ejemplos: iconos, el titular de una página, un botón o la imagen principal de un post.

  A nivel de código comprenden etiquetas HTML como las propias de los encabezados, imágenes, párrafos, botones, etiquetas de formularios o tablas.

  Pero, un átomo carece de significado si no forma parte de un sistema mayor.

- MOLECULAS:

  Son agrupaciones de átomos que, unidos, adquieren nuevas características y funcionan como una unidad. Forman componentes de interfaz relativamente simples, como un menú, una tabla o un formulario de búsqueda.

  A través de la reutilización de las moléculas, allí donde se necesiten, eliminamos complejidad, facilitamos el testado y aseguramos la coherencia gráfica, estructural y conceptual de la interfaz.

- ORGANISMOS

  Son conjuntos de moléculas y átomos que crean elementos más complejos de una interfaz.

  Por ejemplo, una cabecera contiene moléculas y átomos, como un logotipo, la navegación principal y de su sección, la selección del idioma o un formulario de búsqueda.

  Organismos son elementos reutilizados asiduamente como cabeceras y pies, un menú completo de navegación o un listado de elementos.

- PLANTILLAS

  Las plantillas son el último nivel de abstracción del sistema. En ellas se disponen los diversos componentes que hemos visto de forma jerarquizada para conformar una estructura de contenidos determinada.

- PAGINAS

  Las páginas son instancias específicas de las plantillas, creadas aplicando contenidos reales.

  En las páginas todos los componentes se conjuntan para crear un efectivo producto digital.

# Ejemplos de Design Systems

- https://carbondesignsystem.com/

- https://design.facebook.com/

- https://atlassian.design/

# CSS Architecture Checklist

- Organized - Fixed code structure
- No specifity issues
- Atomic Design Principles
- Easy to Understand (comments, variables)
- Fully customizable / themeable
- Reusable across teams / projects
